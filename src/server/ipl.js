exports.team_Won_Toss_And_Matches = function (matches) {
  // By using Higher order function

  const result = matches
    .filter((match) => {
      if (match.toss_winner === match.winner) {
        return true;
      }
    })
    .map((team) => {
      return team.winner;
    })
    .reduce(function (acc, curr) {
      if (typeof acc[curr] == "undefined") {
        acc[curr] = 1;
      } else {
        acc[curr] += 1;
      }

      return acc;
    }, {});

  return Object.entries(result);
};


exports.player_Of_The_Match_Per_Season = function (matches) {
  //  Using higher order function

  const countAllMomPerSeason = matches.reduce((acc, curr) => {
    if (acc[curr.season]) {
      if (acc[curr.season][curr.player_of_match]) {
        acc[curr.season][curr.player_of_match] += 1;
      } else {
        acc[curr.season][curr.player_of_match] = 1;
      }
    } else {
      acc[curr.season] = {};
    }
    return acc;
  }, {});
  const playerOfTheMatchPerSeason = Object.entries(countAllMomPerSeason).map(
    (year) => {
      const player = Object.keys(year[1]).reduce((a, b) =>
         year[1][a] >= year[1][b] ? a : b
       );
      return [year[0]+` (${player})`, year[1][player]]
    }
  );
  return playerOfTheMatchPerSeason;
};


exports.strike_Rate_Of_A_Batsman_In_Each_Season = function (
  matches,
  deliveries, 
  playerName
) {
  const strikeRate = matches.reduce((startingValue, currentValue) => {
    const countBallsAndRuns = deliveries.reduce((acc, curr) => {
      if (curr.match_id == currentValue.id && curr.batsman == playerName) {
        if (acc[currentValue.season]) {
          acc[currentValue.season] = [
            acc[currentValue.season][0],
            acc[currentValue.season][1] + parseInt(curr.batsman_runs),
          ];
          if (parseInt(curr.wide_runs) + parseInt(curr.noball_runs) === 0) {
            acc[currentValue.season] = [
              (acc[currentValue.season][0] += 1),
              acc[currentValue.season][1],
            ];
          }
        } else {
          acc[currentValue.season] = [0, parseInt(curr.batsman_runs)];
          if (parseInt(curr.wide_runs) + parseInt(curr.noball_runs) === 0) {
            acc[currentValue.season] = [1, acc[currentValue.season][1]];
          }
        }
      }
      return acc;
    }, {});
    const key = Object.keys(countBallsAndRuns);
    const year = key[0];
    if (countBallsAndRuns[year] != undefined) {
      if (startingValue[year]) {
        startingValue[year] = [
          startingValue[year][0] + countBallsAndRuns[year][0],
          startingValue[year][1] + countBallsAndRuns[year][1],
        ];
      } else {
        startingValue[year] = [
          countBallsAndRuns[year][0],
          countBallsAndRuns[year][1],
        ];
      }
    }
    return startingValue;
  }, {});
  const dataInto2d = Object.entries(strikeRate);
  const result = dataInto2d.map((data) => {
    let calc = ((data[1][1] / data[1][0]) * 100).toFixed(2);
    return [data[0], calc];
  });
  return result;
};


exports.best_Economy_In_Super_Overs = function (deliveries) {
  // By using higher order functions

  const calculateBestEconomy = deliveries
    .filter((del) => {
      return parseInt(del.is_super_over);
    })
    .reduce((acc, curr) => {
      if (acc[curr.bowler]) {
        acc[curr.bowler] = [
          acc[curr.bowler][0],
          (acc[curr.bowler][1] +=
            parseInt(curr.batsman_runs) +
            parseInt(curr.noball_runs) +
            parseInt(curr.wide_runs)),
        ];
        if (parseInt(curr.wide_runs) + parseInt(curr.noball_runs) === 0) {
          acc[curr.bowler] = [(acc[curr.bowler][0] += 1), acc[curr.bowler][1]];
        }
      } else {
        acc[curr.bowler] = [
          0,
          parseInt(curr.batsman_runs) +
            parseInt(curr.noball_runs) +
            parseInt(curr.wide_runs),
        ];
        if (parseInt(curr.wide_runs) + parseInt(curr.noball_runs) === 0) {
          acc[curr.bowler] = [1, acc[curr.bowler][1]];
        }
      }
      return acc;
    }, {});
  const dataInto2d = Object.entries(calculateBestEconomy).reduce(
    (acc, curr) => {
      acc[curr[0]] = (curr[1][1] / (curr[1][0] / 6).toString()).toFixed(2);
      return acc;
    },
    {}
  );
  const bestEconomy = Object.entries(dataInto2d);
  bestEconomy.sort((a, b) => {
    return parseInt(a[1]) - parseInt(b[1]);
  });
  return bestEconomy[0];
};


exports.player_Dismissed_By_Another_Player = (deliveries, playerName) => {
  // By using higher order function

  const allBowlerWhoDismissedTheBatsman = deliveries
    .filter((del) => {
      if (del.player_dismissed === playerName) {
        return true;
      }
    })
    .map((data) => {
      return data.bowler;
    })
    .reduce(function (acc, curr) {
      if (typeof acc[curr] == "undefined") {
        acc[curr] = 1;
      } else {
        acc[curr] += 1;
      }

      return acc;
    }, {});
  let topWicketTakerBowlers = Object.entries(allBowlerWhoDismissedTheBatsman)
    .sort((a, b) => {
      return b[1] - a[1];
    })
    .slice(0, 10)
    .reduce((acc, curr) => {
      acc[curr[0]] = curr[1];
      return acc;
    }, {});
  return Object.entries(topWicketTakerBowlers);
};
