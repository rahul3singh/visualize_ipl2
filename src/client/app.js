async function fetchData(path) {
  const jsonData = await fetch(path);
  return jsonData.json();
}

async function getData() {
  const teamWonTossAndMatches = await fetchData(
    "./output/team_Won_Toss_And_Matches.json"
  );
  visualizeTeamWonTossAndMatches(
    teamWonTossAndMatches["team_Won_Toss_And_Matches"]
  );
  const playerOfTheMatch = await fetchData(
    "./output/player_Of_The_Match_Per_Season.json"
  );
  visualizePlayerOfTheSeson(playerOfTheMatch["player_Of_The_Match_Per_Season"]);
  const mostDismissedMsDhoniByBowlers = await fetchData(
    "./output/most_Dismissed_Ms_dhoni_By_A_Bowler.json"
  );
  visualizeTopBowlersWhoDismissedThePlayer(
    mostDismissedMsDhoniByBowlers["most_Dismissed_Ms_dhoni_By_A_Bowler"]
  );
  const strikeRateInEachSeason = await fetchData(
    "./output/strike_Rate_Of_MS_Dhoni__In_Each_Season.json"
  );
  visualizeStrikeRateOfBatsman(
    strikeRateInEachSeason["strike_Rate_Of_MS_Dhoni__In_Each_Season"]
  );
}

getData();

function convertDataIntoHighchartFormat(jsonData) {
  const arr = jsonData.map((data) => {
    const obj = {};
    obj["name"] = data[0];
    if (data[1] != parseInt(data[1])) {
      obj["y"] = parseFloat(data[1]);
    } else {
      obj["y"] = parseInt(data[1]);
    }
    return obj;
  });
  const result = [
    {
      colorByPoint: true,
      data: arr,
    },
  ];
  return result;
}
//************************************************* // team Won Toss and Match // *****************************************************************
function visualizeTeamWonTossAndMatches(matchTossWon) {
  const result = convertDataIntoHighchartFormat(matchTossWon);
  Highcharts.chart("team_Won_Toss_Match", {
    chart: {
      type: "column",
    },
    title: {
      text: "Number of times each team won the toss and also won the match",
    },
    subtitle: {
      text:
        'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>',
    },
    accessibility: {
      announceNewData: {
        enabled: true,
      },
    },
    xAxis: {
      type: "category",
    },
    yAxis: {
      title: {
        text: "Matches Won",
      },
    },
    legend: {
      enabled: false,
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: "{point.y:.0f}",
        },
      },
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>Won Both Toss And Match :{point.y:.0f} times</b><br/>',
    },

    series: result,
  });
}

//************************************************ // Player of the match // ************************************************************************
function visualizePlayerOfTheSeson(playerOfTheMatch) {
  Highcharts.chart("player_Of_The_Match", {
    chart: {
      type: "column",
    },
    title: {
      text: "Highest Player Of The Match Per season",
    },
    subtitle: {
      text:
        'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>',
    },
    xAxis: {
      type: "category",
      labels: {
        rotation: -45,
        style: {
          fontSize: "13px",
          fontFamily: "Verdana, sans-serif",
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Number of times Player Of The Match",
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: "Player Of The Match",
    },
    series: [
      {
        name: "PLayer Of The Match Per Season",
        data: playerOfTheMatch,
        dataLabels: {
          enabled: true,
          rotation: 0,
          color: "#FFFFFF",
          align: "right",
          format: "{point.y:.0f}", // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
    ],
  });
}

// **************************************************** // Most Dismissed Batsman By Bowlers// ************************************************************************8
function visualizeTopBowlersWhoDismissedThePlayer(mostDismissed) {
  Highcharts.chart("most_Dismissed", {
    chart: {
      type: "column",
    },
    title: {
      text: "Most Dismissed MS DHONI By A Bowler",
    },
    subtitle: {
      text:
        'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>',
    },
    xAxis: {
      type: "category",
      labels: {
        rotation: 0,
        style: {
          fontSize: "13px",
          fontFamily: "Verdana, sans-serif",
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Wickets Taken",
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: "Dismissed Ms_Dhoni",
    },
    series: [
      {
        name: "Wickets",
        data: mostDismissed,
        dataLabels: {
          enabled: true,
          rotation: 0,
          color: "#FFFFFF",
          align: "right",
          format: "{point.y:.0f}", // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
    ],
  });
}

// *************************************// Strike Rate Of Ms Dhoni In Each Year // *************************************************8888
function visualizeStrikeRateOfBatsman(strikeRateData) {
  const result = convertDataIntoHighchartFormat(strikeRateData);
  // Create the chart
  Highcharts.chart("strike_Rate", {
    chart: {
      type: "column",
    },
    title: {
      text: "Strike Rate of MS Dhoni from 2008 to 2017",
    },
    subtitle: {
      text:
        'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>',
    },
    accessibility: {
      announceNewData: {
        enabled: true,
      },
    },
    xAxis: {
      type: "category",
    },
    yAxis: {
      title: {
        text: "Strike Rate in each year",
      },
    },
    legend: {
      enabled: false,
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: "{point.y:.2f}",
        },
      },
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>Strike Rate Of Ms Dhoni :{point.y:.0f}</b><br/>',
    },

    series: result,
  });
}
